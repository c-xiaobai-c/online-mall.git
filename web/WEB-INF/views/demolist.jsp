<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/7/8
  Time: 22:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table border="1" width="80%">
    <Tr>
        <th>id</th>
        <th>username</th>
        <th>age</th>
    </Tr>
    <c:forEach items="${demolist}" var="demo">
        <tr>
            <td>${demo.id}</td>
            <td>${demo.userName}</td>
            <td>${demo.age}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
