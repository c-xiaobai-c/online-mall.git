package com.chen.dao;


import com.chen.entity.Demo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DemoDao {

    public int insert(Demo demo);
    public int delete(Integer id);
    public int update(Demo demo);
    public List<Demo> query(Demo demo);
}
