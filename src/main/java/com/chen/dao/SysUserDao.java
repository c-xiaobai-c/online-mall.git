package com.chen.dao;


import com.chen.entity.Product;
import com.chen.entity.ProductType;
import com.chen.entity.SysRole;
import com.chen.entity.SysUser;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface SysUserDao {

    List<SysUser> findUserByLoginNameAndPassword(Map<String,Object> map);

    List<ProductType>   queryProductType();

    int insert(ProductType productType);

    ProductType onlyProductType(int id);

    int modifyName(ProductType productType);

    int deleteProduct(int id);

    int modifyStatus(ProductType productType);

    List<SysUser> querySysUser();

    int addSysUser(SysUser sysUser);

    List<SysRole> querySysRole();

    SysUser queryOnlySysUser(int id);

    int updateSysUser(SysUser sysUser);

    int updateSysUserModifyStatus(SysUser sysUser);

    List<SysUser> findSysUSerById(SysUser sysUser);

   List<Product> queryProduct();

   int addProduct(Product product);

   Product queryProductById(int id);

   int updateProduct(Product product);

   int deleteProductBYId(int id);
}
