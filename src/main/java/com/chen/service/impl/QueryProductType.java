package com.chen.service.impl;

import com.chen.entity.Product;
import com.chen.entity.ProductType;
import com.chen.entity.SysRole;
import com.chen.entity.SysUser;

import java.util.List;

public interface QueryProductType {

    List<ProductType> queryProductType();
    int insert(ProductType productType);
    ProductType onlyProductType(int id);
    int modifyName(ProductType productType);
    int deleteProduct(int id);
    int modifyStatus(ProductType productType);
    List<SysUser> querySysUser();
    int  addSysUser(SysUser sysUser);
    List<SysRole> querySysRole();
    SysUser queryOnlySysUser(int id);
    int updateSysUser(SysUser sysUser);
    int updateSysUserModifyStatus(SysUser sysUser);
    List<SysUser> findSysUSerById(SysUser sysUser);
    List<Product> queryProduct();
    int addProduct(Product product);
    Product queryProductById(int id);
    int updateProduct(Product product);
    int deleteProductBYId(int id);
}
