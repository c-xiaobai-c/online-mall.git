package com.chen.service.impl;


import com.chen.dao.SysUserDao;
import com.chen.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SysUserSerivceImpl implements SysUserSerivce {

    @Autowired
    private SysUserDao sysUserDao;

    @Override
    public SysUser login(Map<String,Object> map) {

        List<SysUser> sysUserList=sysUserDao.findUserByLoginNameAndPassword(map);

        if(sysUserList!=null && sysUserList.size()>0)
        {
            //此用户存在，则登录成功
            return sysUserList.get(0);
        }else {
            return null;
        }
    }


}