package com.chen.service.impl;


import com.chen.dao.DemoDao;
import com.chen.entity.Demo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DemoServiceImpl implements DemoService {

    @Autowired
    private DemoDao demoDao;


    @Override
    public List<Demo> query(Demo demo) {
        return demoDao.query(demo);
    }
}
