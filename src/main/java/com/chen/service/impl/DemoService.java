package com.chen.service.impl;

import com.chen.entity.Demo;

import java.util.List;

public interface DemoService {
    public List<Demo> query(Demo demo) ;
}
