package com.chen.service.impl;

import com.chen.dao.SysUserDao;
import com.chen.entity.Product;
import com.chen.entity.ProductType;
import com.chen.entity.SysRole;
import com.chen.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class QueryProductTypeImpl  implements QueryProductType{


    @Autowired
    private SysUserDao sysUserDao;

    @Override
    public List<ProductType> queryProductType() {
        return sysUserDao.queryProductType();
    }

    @Override
    public int insert(ProductType productType) {
        return sysUserDao.insert(productType);
    }

    @Override
    public ProductType onlyProductType(int id) {
        return sysUserDao.onlyProductType(id);
    }

    @Override
    public int modifyName(ProductType productType) {
        return sysUserDao.modifyName(productType);
    }

    @Override
    public int deleteProduct(int id) {
        return sysUserDao.deleteProduct(id);
    }

    @Override
    public int modifyStatus(ProductType productType) {
        return sysUserDao.modifyStatus(productType);
    }

    @Override
    public List<SysUser> querySysUser(){
        return sysUserDao.querySysUser();
    }

    @Override
    public List<SysRole> querySysRole() {
        return sysUserDao.querySysRole();
    }

    @Override
    public SysUser queryOnlySysUser(int id) {
        return sysUserDao.queryOnlySysUser(id);
    }

    @Override
    public int updateSysUser(SysUser sysUser) {
        return sysUserDao.updateSysUser(sysUser);
    }

    @Override
    public int updateSysUserModifyStatus(SysUser sysUser) {
        return sysUserDao.updateSysUserModifyStatus(sysUser);
    }

    @Override
    public List<SysUser> findSysUSerById(SysUser sysUser) {
        return sysUserDao.findSysUSerById(sysUser);
    }

    @Override
    public List<Product> queryProduct() {
        return sysUserDao.queryProduct();
    }

    @Override
    public int addProduct(Product product) {
        return sysUserDao.addProduct(product);
    }

    @Override
    public Product queryProductById(int id) {
        return sysUserDao.queryProductById(id);
    }

    @Override
    public int updateProduct(Product product) {
        return sysUserDao.updateProduct(product);
    }

    @Override
    public int deleteProductBYId(int id) {
        return sysUserDao.deleteProductBYId(id);
    }


    @Override
    public int  addSysUser(SysUser sysUser) {
        return sysUserDao.addSysUser(sysUser);
    }


}
