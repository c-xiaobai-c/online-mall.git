package com.chen.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
/*商品表*/
public class Product {

    private int id;
    private String name;//商品名称
    private double price;//价格
    private String info;//简介
    private String image;//图片
    private int product_type_id;//类型id
    private ProductType productType;// 商品类型表


}
