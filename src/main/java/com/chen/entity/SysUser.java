package com.chen.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/*
* -- 系统用户表
create table t_sysuser (
  id int primary key auto_increment,
  name varchar(50),
  login_name varchar(50) not null unique,
  password varchar(50),
  phone varchar(50),
  email varchar(100),
  is_valid int,-- 1表示有效，0表示无效
  create_date datetime,
  role_id int,
  foreign key(role_id) references t_role(id)
)engine=Innodb default charset=utf8;
* */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysUser {

    private Integer id;
    private String name;
    private String loginName;
    private String password;
    private String phone;
    private String email;
    private Integer isValid;
    private Date createDate;
    //定义一个role对象（体现一对一关系）
    private SysRole sysRole;
    private  Integer roleId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIsValid() {
        return isValid;
    }

    public void setIsValid(Integer isValid) {
        this.isValid = isValid;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public SysRole getSysRole() {
        return sysRole;
    }

    public void setSysRole(SysRole sysRole) {
        this.sysRole = sysRole;
    }
}