package com.chen.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductType {

    private int id;
    private String name;//类型名称
    private int status;//类型状态，1表示启用，0表示禁用

}
