package com.chen.controller;


import com.chen.entity.Demo;
import com.chen.service.impl.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class DemoController {

    @Autowired
    private DemoService demoService;

    @RequestMapping("/list")
    public String list(Model model)
    {
        Demo demo=new Demo();
        List<Demo> demoList=demoService.query(demo);

        model.addAttribute("demolist",demoList);

        return "demolist";

    }
}