package com.chen.controller;


import com.chen.entity.*;
import com.chen.service.impl.QueryProductType;
import com.chen.service.impl.SysUserSerivce;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.crypto.Data;
import java.io.*;
import java.util.*;

@Controller
//@RequestMapping("/backend/sysuser")
public class SysUserController {

    @Autowired
    private SysUserSerivce sysUserSerivce;

    @Autowired
    private QueryProductType queryProductType;

    @RequestMapping("/toLogin")
    public String login(SysUser sysUser, Model model) {
//        SysUser sysUser1 = sysUserSerivce.login(sysUser);
//
//        if (sysUser1 != null) {
//            //则登录认证成功
//            return "main";
//        } else {
//            //登录失败返回登录页
//            model.addAttribute("errorMsg", "请重新输入登录名及密码!");
//            return "login";
//        }
        return "login";
    }

    @RequestMapping("/login")
    @ResponseBody
    public String login(HttpSession session, String account,String password,String code) throws Exception {
        // 向session记录用户身份信息

        System.out.println("===========");
        System.out.println("==========="+code);

        if(session.getAttribute("verifyCode")!=null) {
            if(session.getAttribute("verifyCode").equals(code)) {
                session.removeAttribute("verifyCode");
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("loginName",account);
                map.put("password",password);
                SysUser sysUser1 = sysUserSerivce.login(map);
             //   System.out.println("==========="+sysUser1);
                session.setAttribute("user", sysUser1);
                if (account.equals("") || password.equals("")) {
                  //  System.out.println("=========");
                    return "账号不能为空或密码不能为空";
                } else if (sysUser1 != null) {
                      System.out.println("成功");
                  //  int n = loginService.addFrequency(map);  //添加登录次数，以及登录时间
                 //   System.out.println("login===========" + n);
                    return "成功";

                } else {
                    return "失败";

                }
            }
            else{
                session.removeAttribute("verifyCode");
                return "验证码错误";
            }
        }
        else {
            return "错误";

        }
      //  return null;
    }

    @RequestMapping("/toMain")
    public String toMain(Model model){




        return "/main";
    }
    @RequestMapping("/toProductTypeManager")
    public String toProductTypeManager(Integer pageNum,Model model){
      /*  List<ProductType> productTypeList=queryProductType.queryProductType();
      //  System.out.println(productTypeList);
        model.addAttribute("productTypeList",productTypeList);*/

        if(pageNum==null)
        {
            pageNum=1;
        }

        //初始化PagerHepler插件
        PageHelper.startPage(pageNum, 8);

        List<ProductType> productTypeList=queryProductType.queryProductType();

        //使用PagerHelper插件
        PageInfo<ProductType> pageInfo=new PageInfo<>(productTypeList);

        model.addAttribute("pageInfo",pageInfo);
      /*  for (ProductType type : productTypeList) {

            System.out.println(type);
        }*/

        return "/productTypeManager";
    }

    @RequestMapping("getcode")
    @ResponseBody
    public String getcode(HttpSession session) throws Exception {
        // 向session记录验证码
        String verifyCode = String.valueOf(new Random().nextInt(89999) + 10000);//生成5位验证码
        session.setAttribute("verifyCode",verifyCode);
        return verifyCode;
    }


    @RequestMapping("/add")
    @ResponseBody
    public ResponseResult add(ProductType productType)
    {
        productType.setStatus(1);
        int flag=queryProductType.insert(productType);

        if(flag>0)
        {
            return ResponseResult.success();
        }else
            return ResponseResult.fail();
    }

    //退出登陆
    @RequestMapping("/logout")
    public String logout(HttpSession session) throws Exception {
        // session 过期
        session.invalidate();
        return "login";
    }


   @RequestMapping("/findById")
    @ResponseBody
    public ResponseResult onlyProductType(int id){

        System.out.println("dddddddddd"+id);
        ProductType productType=queryProductType.onlyProductType(id);
       System.out.println(productType);
     //  ResponseResult responseResult=new ResponseResult();
      // responseResult.setStatus(1);
     //  responseResult.setData(productType);


      return ResponseResult.success(productType);
    }

    @RequestMapping("/modifyName")
    @ResponseBody
    public ResponseResult modifyName(ProductType productType){

        System.out.println(productType);
        int n=queryProductType.modifyName(productType);
        if(n>0){
            System.out.println(n+"123");
            return  ResponseResult.success();
        }
        else {
            System.out.println(n+"123");
            return  ResponseResult.fail();
        }


    }



    @RequestMapping("/removeById")
    @ResponseBody
    public ResponseResult removeById(int id){
        int n=queryProductType.deleteProduct(id);
        if(n>0){
            System.out.println(n+"123");
            return  ResponseResult.success();
        }
        else {
            System.out.println(n+"123");
            return  ResponseResult.fail();
        }
    }

    @RequestMapping("/modifyStatus")
    @ResponseBody
    public ResponseResult modifyStatus(int id){
        ProductType productType=queryProductType.onlyProductType(id);
        System.out.println(productType);
        if(productType.getStatus()==1){
            System.out.println("==========");
            productType.setStatus(0);
            productType.setId(id);
            int n=queryProductType.modifyStatus(productType);
            if (n>0){
                System.out.println("00000000000");
                return  ResponseResult.success();
            }
            else {
                return  ResponseResult.fail();
            }
        }
        else {
            productType.setStatus(1);
            productType.setId(id);
            int n=queryProductType.modifyStatus(productType);
            if (n>0){
                return  ResponseResult.success();
            }
            else {
                return  ResponseResult.fail();
            }
        }


    }




    /*
    * 用户管理
    * */

    @RequestMapping("/toSysUserManager")
    public String toSysUserManager(Integer pageNum,Model model){

        if(pageNum==null)
        {
            pageNum=1;
        }
        //初始化PagerHepler插件
        PageHelper.startPage(pageNum, 5);

        List<SysUser> sysUserList=queryProductType.querySysUser();

        System.out.println(sysUserList+"===");


        //使用PagerHelper插件
     PageInfo<SysUser> pageInfo=new PageInfo<>(sysUserList);

//     List<SysUser> sysUserList1=pageInfo.getList();
//     for(SysUser sysUser:sysUserList1){
//         System.out.println(sysUser);
//     }

     List<SysRole> sysRoles=queryProductType.querySysRole();
     System.out.println(sysRoles);

       model.addAttribute("pageInfo",pageInfo);
       model.addAttribute("roles",sysRoles);
        return "sysuserManager";
    }


    @RequestMapping("/addSysUser")
    @ResponseBody
    public  ResponseResult addSysUser(SysUser sysUser){


        System.out.println("aaaa"+sysUser);
        int n=queryProductType.addSysUser(sysUser);
       if(n>0){
           return  ResponseResult.success();
       }
       else {
           return   ResponseResult.fail();
       }




    }


    @RequestMapping("/updateSysUser")
    @ResponseBody
    public  ResponseResult updateSysUser(SysUser sysUser){
        System.out.println(sysUser);
        int n=queryProductType.updateSysUser(sysUser);
        if(n>0){
           // System.out.println("成功");
            return  ResponseResult.success();
        }
        else {
            return   ResponseResult.fail();
        }

    }

     @RequestMapping("/findByUserId")
    @ResponseBody
    public ResponseResult findByUserId(int id){
       SysUser sysUser= queryProductType.queryOnlySysUser(id);
       System.out.println(sysUser);
       System.out.println(sysUser.getSysRole().getRoleName());
         return ResponseResult.success(sysUser);

     }

     @RequestMapping("/sysuser/modifyStatus")
    @ResponseBody
    public ResponseResult SysUserModifyStatus(int id){
         SysUser sysUser= queryProductType.queryOnlySysUser(id);
          if(sysUser.getIsValid()==1){
              sysUser.setIsValid(0);
              sysUser.setId(id);
              int n=queryProductType.updateSysUserModifyStatus(sysUser);
              if(n>0){
                  return ResponseResult.success();
              }
              else {
                  return   ResponseResult.fail();
              }
          }else{
              sysUser.setIsValid(1);
              sysUser.setId(id);
              int n=queryProductType.updateSysUserModifyStatus(sysUser);
              if(n>0){
                  return ResponseResult.success();
              }
              else {
                  return   ResponseResult.fail();
              }
         }

     }

     @RequestMapping("/findByParams")
     public String findByParams(SysUser sysUser,Integer pageNum,Model model){
        System.out.println(sysUser);
         if(pageNum==null)
         {
             pageNum=1;
         }
         //初始化PagerHepler插件
         PageHelper.startPage(pageNum, 5);
         List<SysUser> sysUserList=queryProductType.findSysUSerById(sysUser);
         System.out.println(sysUserList);
         //使用PagerHelper插件
         PageInfo<SysUser> pageInfo=new PageInfo<>(sysUserList);
         List<SysRole> sysRoles=queryProductType.querySysRole();
         model.addAttribute("roles",sysRoles);

         model.addAttribute("sysuserParam",sysUser);
         model.addAttribute("pageInfo",pageInfo);



        return "sysuserManager";
     }

     @RequestMapping("/toProductManager")
    public String toProductManager(Integer pageNum,Model model){

         if(pageNum==null)
         {
             pageNum=1;
         }
         //初始化PagerHepler插件
         PageHelper.startPage(pageNum, 5);

        List<Product> productList=queryProductType.queryProduct();
        System.out.println(productList);

         //使用PagerHelper插件
         PageInfo<Product> pageInfo=new PageInfo<>(productList);

         List<ProductType> productTypeList=queryProductType.queryProductType();
         model.addAttribute("pageInfo",pageInfo);
         model.addAttribute("productTypes",productTypeList);

        return "productManager";
     }

     @RequestMapping("/product/add")
     public String ProductAdd(@RequestParam("file") CommonsMultipartFile file, Product product , HttpServletRequest request,Model model) throws IOException {

         System.out.println(product);


         //获取文件名 : file.getOriginalFilename();
         String uploadFileName = file.getOriginalFilename();

         //如果文件名为空，直接回到首页！
         if ("".equals(uploadFileName)) {
             // return "redirect:/toProductManager";
             System.out.println("没有图片");
         } else {
             System.out.println("上传文件名 : " + uploadFileName);

             //上传路径保存设置
             String path = request.getServletContext().getRealPath("/upload");
             //如果路径不存在，创建一个
             File realPath = new File(path);
             if (!realPath.exists()) {
                 realPath.mkdir();
             }
             System.out.println("上传文件保存地址：" + realPath);

             InputStream is = file.getInputStream(); //文件输入流
             OutputStream os = new FileOutputStream(new File(realPath, uploadFileName)); //文件输出流

             //读取写出
             int len = 0;
             byte[] buffer = new byte[1024];
             while ((len = is.read(buffer)) != -1) {
                 os.write(buffer, 0, len);
                 os.flush();
             }
             os.close();
             is.close();
             product.setImage("/upload/"+uploadFileName);
         }


         try {
             int n= queryProductType.addProduct(product);
             if(n>0){
                 System.out.println("添加商品成功");
                 model.addAttribute("sucessMsg","添加成功");
             }
             else {
                 model.addAttribute("sucessMsg","添加失败");
             }

         } catch (Exception e) {
             e.printStackTrace();
             model.addAttribute("errorMsg","添加失败,商品名字重复");
         }

          //   return "redirect:/toProductManager";


         return toProductManager(Integer.MAX_VALUE,model);
         }



         @RequestMapping("product/findById")
         @ResponseBody
       public ResponseResult ProductFindById(int id){
             System.out.println("id"+id);
        Product product=queryProductType.queryProductById(id);
        System.out.println(product);
        if(product!=null){
            return ResponseResult.success(product);
        }
        else {
            return ResponseResult.fail();
        }

         }

         @RequestMapping("/product/modify")
        public String ProductMOdify(@RequestParam("file") CommonsMultipartFile file,Product product,int pageNum,Model model, HttpServletRequest request) throws IOException {

             System.out.println(product);


             //获取文件名 : file.getOriginalFilename();
             String uploadFileName = file.getOriginalFilename();

             //如果文件名为空，直接回到首页！
             if ("".equals(uploadFileName)) {
                 // return "redirect:/toProductManager";
                 System.out.println("没有图片");
             } else {
                 System.out.println("上传文件名 : " + uploadFileName);

                 //上传路径保存设置
                 String path = request.getServletContext().getRealPath("/upload");
                 //如果路径不存在，创建一个
                 File realPath = new File(path);
                 if (!realPath.exists()) {
                     realPath.mkdir();
                 }
                 System.out.println("上传文件保存地址：" + realPath);

                 InputStream is = file.getInputStream(); //文件输入流
                 OutputStream os = new FileOutputStream(new File(realPath, uploadFileName)); //文件输出流

                 //读取写出
                 int len = 0;
                 byte[] buffer = new byte[1024];
                 while ((len = is.read(buffer)) != -1) {
                     os.write(buffer, 0, len);
                     os.flush();
                 }
                 os.close();
                 is.close();
                 product.setImage("/upload/"+uploadFileName);
             }

             if(product.getImage()==null){
                 Product product2=queryProductType.queryProductById(product.getId());
                 product.setImage(product2.getImage());
             }

        int n= queryProductType.updateProduct(product);
        if(n>0){
            System.out.println("修改成功");
        }

           //  return "redirect:/toProductManager";
             return toProductManager(pageNum,model);
         }


         @RequestMapping("/product/removeById")
         @ResponseBody
        public ResponseResult ProductRemoveById(int id){

        int n=queryProductType.deleteProductBYId(id);
        if(n>0 ){
            return  ResponseResult.success();
        }
        else {

            return ResponseResult.fail();

        }

         }


         @RequestMapping("toError")
    public String toError(){
        return "error";
         }

 }