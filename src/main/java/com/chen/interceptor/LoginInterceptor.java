package com.chen.interceptor;


import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginInterceptor implements HandlerInterceptor {

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws ServletException, IOException {
        // 如果是登陆页面则放行
        System.out.println("uri: " + request.getRequestURI());
        HttpSession session = request.getSession();
        if(request.getRequestURI().contains("/login")||request.getRequestURI().contains("/getcode")) {
            System.out.println("11111111111111111111");
            return true;
        }

        // 如果用户已登陆也放行
        else if(session.getAttribute("user") != null){
            System.out.println("22222222222");
            return true;
        }

        // 用户没有登陆跳转到登陆页面
        else{
            System.out.println("333333333333");
            request.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(request, response);
        }
        return false;
    }

    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        System.out.println("------------处理后------------");

    }

    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        System.out.println("------------清理------------");

    }
}