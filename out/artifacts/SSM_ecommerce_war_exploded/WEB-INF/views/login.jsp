<%--
  Created by IntelliJ IDEA.
  User: 86151
  Date: 2021/7/9
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <title>在线商城-后台管理系统</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/bootstrap.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/mycss.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/bootstrapValidator.min.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/statics/js/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/statics/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/statics/js/bootstrapValidator.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/statics/layer/layer.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/zshop.css" />
    <script src="${pageContext.request.contextPath}/statics/js/jquery-3.6.0.js"></script>
    <script>
        //重新加载验证码
        function reloadImage(){
            $('#randCode').attr('src','${pageContext.request.contextPath}/backend/code/image?time='+new Date().getTime());
            $('#code').val('');
        }

        $(function(){
            $('#frmLogin').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields:{
                    loginName:{
                        validators:{
                            notEmpty:{
                                message:'用户名不能为空'
                            }
                        }
                    },
                    password:{
                        validators:{
                            notEmpty:{
                                message:'密码不能为空'
                            }
                        }
                    }/*,
                    code:{
                        validators:{
                            notEmpty:{
                                message:'请输入验证码'
                            },
                            remote:{
                                url:'${pageContext.request.contextPath}/backend/code/checkCode',
                                message:'验证码不正确'
                            }
                        }
                    }*/
                }
            });

            //服务端提示消息
            let errorMsg='${errorMsg}';
            if(errorMsg!=''){
                layer.msg(errorMsg,{
                    time:2000,
                    skin:'errorMsg'
                });
            }
        });

    </script>
</head>
<body>
<!-- 使用自定义css样式 div-signin 完成元素居中-->
<div class="container div-signin">
    <div class="panel panel-primary div-shadow">
        <!-- h3标签加载自定义样式，完成文字居中和上下间距调整 -->
        <div class="panel-heading">
            <h3>在线商城系统 3.0</h3>
            <span>ZSHOP Manager System</span>
        </div>
        <div class="panel-body">
            <!-- login form start -->
          <%--  <form action="${pageContext.request.contextPath}/backend/sysuser/login" class="form-horizontal" method="post" id="frmLogin">--%>
        <div class="form-horizontal" >
                <div class="form-group">
                    <label class="col-sm-3 control-label">用户名：</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" placeholder="请输入用户名" name="loginName" id="account" value="admin">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">密&nbsp;&nbsp;&nbsp;&nbsp;码：</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="password" placeholder="请输入密码" name="password" id="password" value="123">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">验证码：</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" placeholder="验证码" id="code" name="code">
                    </div>
                    <div class="col-sm-2">
                        <!-- 验证码 -->
                     <%--   <img class="img-rounded" src="${pageContext.request.contextPath}/image" id="randCode" style="height: 32px; width: 70px;"/>--%>
                        <input type="submit" id="getcode" style="height:30px;width: 120px;"  onclick="getcode()" value="点击生成验证码">
                    </div>
                   <%-- <div class="col-sm-2">
                        <button type="button" class="btn btn-link" onclick="reloadImage()">看不清</button>
                    </div>--%>
                </div>
                <div class="form-group">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-9 padding-left-0">
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-primary btn-block"onclick="login()">登&nbsp;&nbsp;陆</button>
                        </div>
                        <div class="col-sm-4">
                            <button type="reset" class="btn btn-primary btn-block">重&nbsp;&nbsp;置</button>
                        </div>
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-link btn-block">忘记密码？</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- login form end -->
        </div>
    </div>
</div>
<!-- 页尾 版权声明 -->
<div class="container">
    <div class="col-sm-12 foot-css">
        <p class="text-muted credit">
        </p>
    </div>
</div>



<script>

    function login(){

        $.post("${pageContext.request.contextPath}/login",
            {
                account:$("#account").val(),
                password:$("#password").val(),
                code:$("#code").val()
            },
            function(data,status){

                if(data=="成功") {
                   // alert(data);
                    window.location.assign("${pageContext.request.contextPath}/toMain")
                }
                else{
                    alert(data);
                }
            });
    }
    function getcode(){


        $.post("${pageContext.request.contextPath}/getcode",
            {

            },
            function(data,status){
                $("#getcode").val(data);
            });
    }
</script>

</body>
</html>
